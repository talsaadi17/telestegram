﻿namespace WindowsFormsApplication1
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Bunifu.Framework.UI.BunifuThinButton2 SignButton;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.CodeLabel = new System.Windows.Forms.Label();
            this.CodeText = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.PhoneLabel = new System.Windows.Forms.Label();
            this.PhoneText = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.SignIn = new System.Windows.Forms.Label();
            this.CodeButton = new Bunifu.Framework.UI.BunifuThinButton2();
            this.bunifuSeparator1 = new Bunifu.Framework.UI.BunifuSeparator();
            this.bunifuSeparator2 = new Bunifu.Framework.UI.BunifuSeparator();
            SignButton = new Bunifu.Framework.UI.BunifuThinButton2();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // SignButton
            // 
            SignButton.ActiveBorderThickness = 1;
            SignButton.ActiveCornerRadius = 20;
            SignButton.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(49)))), ((int)(((byte)(60)))));
            SignButton.ActiveForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(177)))), ((int)(((byte)(136)))));
            SignButton.ActiveLineColor = System.Drawing.Color.Transparent;
            SignButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(49)))), ((int)(((byte)(60)))));
            SignButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("SignButton.BackgroundImage")));
            SignButton.ButtonText = "Sign In";
            SignButton.Cursor = System.Windows.Forms.Cursors.Hand;
            SignButton.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            SignButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(177)))), ((int)(((byte)(136)))));
            SignButton.IdleBorderThickness = 1;
            SignButton.IdleCornerRadius = 20;
            SignButton.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(49)))), ((int)(((byte)(60)))));
            SignButton.IdleForecolor = System.Drawing.Color.White;
            SignButton.IdleLineColor = System.Drawing.Color.Transparent;
            SignButton.Location = new System.Drawing.Point(22, 276);
            SignButton.Margin = new System.Windows.Forms.Padding(5);
            SignButton.Name = "SignButton";
            SignButton.Size = new System.Drawing.Size(181, 41);
            SignButton.TabIndex = 1;
            SignButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            SignButton.Click += new System.EventHandler(this.SignButton_Click);
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 5;
            this.bunifuElipse1.TargetControl = this;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.CodeLabel);
            this.panel1.Controls.Add(this.CodeText);
            this.panel1.Controls.Add(this.PhoneLabel);
            this.panel1.Controls.Add(this.PhoneText);
            this.panel1.Controls.Add(this.SignIn);
            this.panel1.Location = new System.Drawing.Point(13, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(367, 322);
            this.panel1.TabIndex = 0;
            // 
            // CodeLabel
            // 
            this.CodeLabel.AutoSize = true;
            this.CodeLabel.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CodeLabel.ForeColor = System.Drawing.Color.DarkGray;
            this.CodeLabel.Location = new System.Drawing.Point(16, 174);
            this.CodeLabel.Name = "CodeLabel";
            this.CodeLabel.Size = new System.Drawing.Size(66, 23);
            this.CodeLabel.TabIndex = 4;
            this.CodeLabel.Text = "Code";
            // 
            // CodeText
            // 
            this.CodeText.BorderColorFocused = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(177)))), ((int)(((byte)(136)))));
            this.CodeText.BorderColorIdle = System.Drawing.Color.DarkGray;
            this.CodeText.BorderColorMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(177)))), ((int)(((byte)(136)))));
            this.CodeText.BorderThickness = 1;
            this.CodeText.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.CodeText.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.CodeText.ForeColor = System.Drawing.Color.DarkGray;
            this.CodeText.isPassword = false;
            this.CodeText.Location = new System.Drawing.Point(17, 204);
            this.CodeText.Margin = new System.Windows.Forms.Padding(4);
            this.CodeText.Name = "CodeText";
            this.CodeText.Size = new System.Drawing.Size(337, 44);
            this.CodeText.TabIndex = 3;
            this.CodeText.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // PhoneLabel
            // 
            this.PhoneLabel.AutoSize = true;
            this.PhoneLabel.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PhoneLabel.ForeColor = System.Drawing.Color.DarkGray;
            this.PhoneLabel.Location = new System.Drawing.Point(16, 79);
            this.PhoneLabel.Name = "PhoneLabel";
            this.PhoneLabel.Size = new System.Drawing.Size(72, 23);
            this.PhoneLabel.TabIndex = 2;
            this.PhoneLabel.Text = "Phone";
            // 
            // PhoneText
            // 
            this.PhoneText.BorderColorFocused = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(177)))), ((int)(((byte)(136)))));
            this.PhoneText.BorderColorIdle = System.Drawing.Color.DarkGray;
            this.PhoneText.BorderColorMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(177)))), ((int)(((byte)(136)))));
            this.PhoneText.BorderThickness = 1;
            this.PhoneText.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.PhoneText.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.PhoneText.ForeColor = System.Drawing.Color.DarkGray;
            this.PhoneText.isPassword = false;
            this.PhoneText.Location = new System.Drawing.Point(17, 109);
            this.PhoneText.Margin = new System.Windows.Forms.Padding(4);
            this.PhoneText.Name = "PhoneText";
            this.PhoneText.Size = new System.Drawing.Size(337, 44);
            this.PhoneText.TabIndex = 1;
            this.PhoneText.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // SignIn
            // 
            this.SignIn.AutoSize = true;
            this.SignIn.Font = new System.Drawing.Font("Century Gothic", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SignIn.ForeColor = System.Drawing.Color.Gainsboro;
            this.SignIn.Location = new System.Drawing.Point(118, 16);
            this.SignIn.Name = "SignIn";
            this.SignIn.Size = new System.Drawing.Size(119, 39);
            this.SignIn.TabIndex = 0;
            this.SignIn.Text = "Sign In";
            // 
            // CodeButton
            // 
            this.CodeButton.ActiveBorderThickness = 1;
            this.CodeButton.ActiveCornerRadius = 20;
            this.CodeButton.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(49)))), ((int)(((byte)(60)))));
            this.CodeButton.ActiveForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(177)))), ((int)(((byte)(136)))));
            this.CodeButton.ActiveLineColor = System.Drawing.Color.Transparent;
            this.CodeButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(49)))), ((int)(((byte)(60)))));
            this.CodeButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CodeButton.BackgroundImage")));
            this.CodeButton.ButtonText = "Send Code";
            this.CodeButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CodeButton.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CodeButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(177)))), ((int)(((byte)(136)))));
            this.CodeButton.IdleBorderThickness = 1;
            this.CodeButton.IdleCornerRadius = 20;
            this.CodeButton.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(49)))), ((int)(((byte)(60)))));
            this.CodeButton.IdleForecolor = System.Drawing.Color.White;
            this.CodeButton.IdleLineColor = System.Drawing.Color.Transparent;
            this.CodeButton.Location = new System.Drawing.Point(188, 276);
            this.CodeButton.Margin = new System.Windows.Forms.Padding(5);
            this.CodeButton.Name = "CodeButton";
            this.CodeButton.Size = new System.Drawing.Size(181, 41);
            this.CodeButton.TabIndex = 2;
            this.CodeButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.CodeButton.Click += new System.EventHandler(this.CodeButton_Click);
            // 
            // bunifuSeparator1
            // 
            this.bunifuSeparator1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(177)))), ((int)(((byte)(136)))));
            this.bunifuSeparator1.LineThickness = 2;
            this.bunifuSeparator1.Location = new System.Drawing.Point(61, 312);
            this.bunifuSeparator1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bunifuSeparator1.Name = "bunifuSeparator1";
            this.bunifuSeparator1.Size = new System.Drawing.Size(98, 15);
            this.bunifuSeparator1.TabIndex = 3;
            this.bunifuSeparator1.Transparency = 255;
            this.bunifuSeparator1.Vertical = false;
            // 
            // bunifuSeparator2
            // 
            this.bunifuSeparator2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(177)))), ((int)(((byte)(136)))));
            this.bunifuSeparator2.LineThickness = 2;
            this.bunifuSeparator2.Location = new System.Drawing.Point(210, 312);
            this.bunifuSeparator2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bunifuSeparator2.Name = "bunifuSeparator2";
            this.bunifuSeparator2.Size = new System.Drawing.Size(132, 15);
            this.bunifuSeparator2.TabIndex = 4;
            this.bunifuSeparator2.Transparency = 255;
            this.bunifuSeparator2.Vertical = false;
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(49)))), ((int)(((byte)(60)))));
            this.ClientSize = new System.Drawing.Size(392, 347);
            this.Controls.Add(this.bunifuSeparator2);
            this.Controls.Add(this.bunifuSeparator1);
            this.Controls.Add(this.CodeButton);
            this.Controls.Add(SignButton);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "LoginForm";
            this.Text = "LoginForm";
            this.Load += new System.EventHandler(this.LoginForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label SignIn;
        private System.Windows.Forms.Label CodeLabel;
        private Bunifu.Framework.UI.BunifuMetroTextbox CodeText;
        private System.Windows.Forms.Label PhoneLabel;
        private Bunifu.Framework.UI.BunifuMetroTextbox PhoneText;
        private Bunifu.Framework.UI.BunifuThinButton2 CodeButton;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator2;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator1;
    }
}