﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.LeftMenu = new System.Windows.Forms.MenuStrip();
            this.MiddleMenu = new System.Windows.Forms.MenuStrip();
            this.Title = new System.Windows.Forms.Label();
            this.MenuButton = new System.Windows.Forms.PictureBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.bunifuSeparator1 = new Bunifu.Framework.UI.BunifuSeparator();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ChatTile1 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.LastMessage1 = new System.Windows.Forms.Label();
            this.LastMessage2 = new System.Windows.Forms.Label();
            this.ChatTile2 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.LastMessage3 = new System.Windows.Forms.Label();
            this.ChatTile3 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.LastMessage4 = new System.Windows.Forms.Label();
            this.ChatTile4 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.LastMessage5 = new System.Windows.Forms.Label();
            this.ChatTile5 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.LastMessage6 = new System.Windows.Forms.Label();
            this.ChatTile6 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.SearchBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.MessageBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuImageButton1 = new Bunifu.Framework.UI.BunifuImageButton();
            this.SendButton = new System.Windows.Forms.Label();
            this.Friend = new System.Windows.Forms.Panel();
            this.Extract = new System.Windows.Forms.PictureBox();
            this.FriendText = new System.Windows.Forms.Label();
            this.FriendPicture = new System.Windows.Forms.PictureBox();
            this.FriendAudio = new System.Windows.Forms.PictureBox();
            this.FriendPic = new System.Windows.Forms.PictureBox();
            this.Me = new System.Windows.Forms.Panel();
            this.SendSteg = new System.Windows.Forms.PictureBox();
            this.MyText = new System.Windows.Forms.Label();
            this.Picture = new System.Windows.Forms.PictureBox();
            this.Audio = new System.Windows.Forms.PictureBox();
            this.SelfPic = new System.Windows.Forms.PictureBox();
            this.ChatName = new System.Windows.Forms.Label();
            this.DropMenu = new System.Windows.Forms.Panel();
            this.bunifuFlatButton2 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton1 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.MessageListener = new System.Windows.Forms.Timer(this.components);
            this.BarPanel = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.MenuButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).BeginInit();
            this.Friend.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Extract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FriendPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FriendAudio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FriendPic)).BeginInit();
            this.Me.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SendSteg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Audio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfPic)).BeginInit();
            this.DropMenu.SuspendLayout();
            this.BarPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // LeftMenu
            // 
            this.LeftMenu.AutoSize = false;
            this.LeftMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(130)))), ((int)(((byte)(163)))));
            this.LeftMenu.Dock = System.Windows.Forms.DockStyle.None;
            this.LeftMenu.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.LeftMenu.Location = new System.Drawing.Point(-1, 0);
            this.LeftMenu.Name = "LeftMenu";
            this.LeftMenu.Size = new System.Drawing.Size(385, 64);
            this.LeftMenu.TabIndex = 0;
            this.LeftMenu.Click += new System.EventHandler(this.LeftMenu_Click);
            this.LeftMenu.MouseLeave += new System.EventHandler(this.LeftMenu_MouseLeave);
            this.LeftMenu.MouseHover += new System.EventHandler(this.LeftMenu_MouseHover);
            // 
            // MiddleMenu
            // 
            this.MiddleMenu.AutoSize = false;
            this.MiddleMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(130)))), ((int)(((byte)(163)))));
            this.MiddleMenu.Dock = System.Windows.Forms.DockStyle.None;
            this.MiddleMenu.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.MiddleMenu.Location = new System.Drawing.Point(384, 0);
            this.MiddleMenu.Name = "MiddleMenu";
            this.MiddleMenu.Size = new System.Drawing.Size(794, 64);
            this.MiddleMenu.TabIndex = 1;
            this.MiddleMenu.MouseLeave += new System.EventHandler(this.MiddleMenu_MouseLeave);
            this.MiddleMenu.MouseHover += new System.EventHandler(this.MiddleMenu_MouseHover);
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(130)))), ((int)(((byte)(163)))));
            this.Title.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.ForeColor = System.Drawing.Color.Transparent;
            this.Title.Location = new System.Drawing.Point(95, 18);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(140, 26);
            this.Title.TabIndex = 2;
            this.Title.Text = "TeleStegram";
            this.Title.Click += new System.EventHandler(this.Title_Click);
            this.Title.MouseLeave += new System.EventHandler(this.Title_MouseLeave);
            this.Title.MouseHover += new System.EventHandler(this.Title_MouseHover);
            // 
            // MenuButton
            // 
            this.MenuButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(130)))), ((int)(((byte)(163)))));
            this.MenuButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.MenuButton.Image = global::WindowsFormsApplication1.Properties.Resources._21;
            this.MenuButton.Location = new System.Drawing.Point(23, 19);
            this.MenuButton.MaximumSize = new System.Drawing.Size(45, 43);
            this.MenuButton.Name = "MenuButton";
            this.MenuButton.Size = new System.Drawing.Size(30, 27);
            this.MenuButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.MenuButton.TabIndex = 3;
            this.MenuButton.TabStop = false;
            this.MenuButton.Click += new System.EventHandler(this.MenuButton_Click);
            this.MenuButton.MouseLeave += new System.EventHandler(this.MenuButton_MouseLeave);
            this.MenuButton.MouseHover += new System.EventHandler(this.MenuButton_MouseHover);
            // 
            // listView1
            // 
            this.listView1.BackColor = System.Drawing.Color.White;
            this.listView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listView1.Location = new System.Drawing.Point(-1, 64);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(385, 625);
            this.listView1.TabIndex = 4;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // bunifuSeparator1
            // 
            this.bunifuSeparator1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator1.ForeColor = System.Drawing.Color.DarkGray;
            this.bunifuSeparator1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.bunifuSeparator1.LineThickness = 2;
            this.bunifuSeparator1.Location = new System.Drawing.Point(379, 64);
            this.bunifuSeparator1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bunifuSeparator1.Name = "bunifuSeparator1";
            this.bunifuSeparator1.Size = new System.Drawing.Size(10, 625);
            this.bunifuSeparator1.TabIndex = 10;
            this.bunifuSeparator1.Transparency = 255;
            this.bunifuSeparator1.Vertical = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Image = global::WindowsFormsApplication1.Properties.Resources.search_3594350_960_720_removebg;
            this.pictureBox1.Location = new System.Drawing.Point(5, 91);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(38, 38);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // ChatTile1
            // 
            this.ChatTile1.Activecolor = System.Drawing.Color.Transparent;
            this.ChatTile1.BackColor = System.Drawing.Color.Transparent;
            this.ChatTile1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ChatTile1.BorderRadius = 0;
            this.ChatTile1.ButtonText = "Yoav Cohen";
            this.ChatTile1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ChatTile1.DisabledColor = System.Drawing.Color.Gray;
            this.ChatTile1.ForeColor = System.Drawing.Color.Black;
            this.ChatTile1.Iconcolor = System.Drawing.Color.White;
            this.ChatTile1.Iconimage = global::WindowsFormsApplication1.Properties.Resources.y_removebg;
            this.ChatTile1.Iconimage_right = null;
            this.ChatTile1.Iconimage_right_Selected = null;
            this.ChatTile1.Iconimage_Selected = null;
            this.ChatTile1.IconMarginLeft = 0;
            this.ChatTile1.IconMarginRight = 0;
            this.ChatTile1.IconRightVisible = true;
            this.ChatTile1.IconRightZoom = 0D;
            this.ChatTile1.IconVisible = true;
            this.ChatTile1.IconZoom = 100D;
            this.ChatTile1.IsTab = false;
            this.ChatTile1.Location = new System.Drawing.Point(-1, 156);
            this.ChatTile1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChatTile1.Name = "ChatTile1";
            this.ChatTile1.Normalcolor = System.Drawing.Color.Transparent;
            this.ChatTile1.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(243)))), ((int)(((byte)(252)))));
            this.ChatTile1.OnHoverTextColor = System.Drawing.Color.Black;
            this.ChatTile1.selected = false;
            this.ChatTile1.Size = new System.Drawing.Size(385, 78);
            this.ChatTile1.TabIndex = 12;
            this.ChatTile1.Text = "Yoav Cohen";
            this.ChatTile1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ChatTile1.Textcolor = System.Drawing.Color.Black;
            this.ChatTile1.TextFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChatTile1.BackColorChanged += new System.EventHandler(this.ChatTile1_BackColorChanged);
            this.ChatTile1.Click += new System.EventHandler(this.ChatTile1_Click);
            // 
            // LastMessage1
            // 
            this.LastMessage1.BackColor = System.Drawing.Color.Transparent;
            this.LastMessage1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.LastMessage1.Location = new System.Drawing.Point(81, 188);
            this.LastMessage1.Name = "LastMessage1";
            this.LastMessage1.Size = new System.Drawing.Size(286, 36);
            this.LastMessage1.TabIndex = 13;
            this.LastMessage1.Text = "checkpoint CSA";
            this.LastMessage1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LastMessage2
            // 
            this.LastMessage2.BackColor = System.Drawing.Color.Transparent;
            this.LastMessage2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.LastMessage2.Location = new System.Drawing.Point(81, 276);
            this.LastMessage2.Name = "LastMessage2";
            this.LastMessage2.Size = new System.Drawing.Size(286, 36);
            this.LastMessage2.TabIndex = 15;
            this.LastMessage2.Text = "Missed call";
            this.LastMessage2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ChatTile2
            // 
            this.ChatTile2.Activecolor = System.Drawing.Color.Transparent;
            this.ChatTile2.BackColor = System.Drawing.Color.Transparent;
            this.ChatTile2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ChatTile2.BorderRadius = 0;
            this.ChatTile2.ButtonText = "Tomer T";
            this.ChatTile2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ChatTile2.DisabledColor = System.Drawing.Color.Gray;
            this.ChatTile2.ForeColor = System.Drawing.Color.Black;
            this.ChatTile2.Iconcolor = System.Drawing.Color.White;
            this.ChatTile2.Iconimage = global::WindowsFormsApplication1.Properties.Resources.t_removebg;
            this.ChatTile2.Iconimage_right = null;
            this.ChatTile2.Iconimage_right_Selected = null;
            this.ChatTile2.Iconimage_Selected = null;
            this.ChatTile2.IconMarginLeft = 0;
            this.ChatTile2.IconMarginRight = 0;
            this.ChatTile2.IconRightVisible = true;
            this.ChatTile2.IconRightZoom = 0D;
            this.ChatTile2.IconVisible = true;
            this.ChatTile2.IconZoom = 91D;
            this.ChatTile2.IsTab = false;
            this.ChatTile2.Location = new System.Drawing.Point(-1, 244);
            this.ChatTile2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChatTile2.Name = "ChatTile2";
            this.ChatTile2.Normalcolor = System.Drawing.Color.Transparent;
            this.ChatTile2.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(243)))), ((int)(((byte)(252)))));
            this.ChatTile2.OnHoverTextColor = System.Drawing.Color.Black;
            this.ChatTile2.selected = false;
            this.ChatTile2.Size = new System.Drawing.Size(385, 78);
            this.ChatTile2.TabIndex = 14;
            this.ChatTile2.Text = "Tomer T";
            this.ChatTile2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ChatTile2.Textcolor = System.Drawing.Color.Black;
            this.ChatTile2.TextFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChatTile2.BackColorChanged += new System.EventHandler(this.ChatTile2_BackColorChanged);
            this.ChatTile2.Click += new System.EventHandler(this.ChatTile2_Click);
            // 
            // LastMessage3
            // 
            this.LastMessage3.BackColor = System.Drawing.Color.Transparent;
            this.LastMessage3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.LastMessage3.Location = new System.Drawing.Point(81, 364);
            this.LastMessage3.Name = "LastMessage3";
            this.LastMessage3.Size = new System.Drawing.Size(286, 36);
            this.LastMessage3.TabIndex = 17;
            this.LastMessage3.Text = "102983310748529";
            this.LastMessage3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ChatTile3
            // 
            this.ChatTile3.Activecolor = System.Drawing.Color.Transparent;
            this.ChatTile3.BackColor = System.Drawing.Color.Transparent;
            this.ChatTile3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ChatTile3.BorderRadius = 0;
            this.ChatTile3.ButtonText = "Elad Ezra";
            this.ChatTile3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ChatTile3.DisabledColor = System.Drawing.Color.Gray;
            this.ChatTile3.ForeColor = System.Drawing.Color.Black;
            this.ChatTile3.Iconcolor = System.Drawing.Color.White;
            this.ChatTile3.Iconimage = global::WindowsFormsApplication1.Properties.Resources.e_removebg;
            this.ChatTile3.Iconimage_right = null;
            this.ChatTile3.Iconimage_right_Selected = null;
            this.ChatTile3.Iconimage_Selected = null;
            this.ChatTile3.IconMarginLeft = 0;
            this.ChatTile3.IconMarginRight = 0;
            this.ChatTile3.IconRightVisible = true;
            this.ChatTile3.IconRightZoom = 0D;
            this.ChatTile3.IconVisible = true;
            this.ChatTile3.IconZoom = 95D;
            this.ChatTile3.IsTab = false;
            this.ChatTile3.Location = new System.Drawing.Point(-1, 332);
            this.ChatTile3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChatTile3.Name = "ChatTile3";
            this.ChatTile3.Normalcolor = System.Drawing.Color.Transparent;
            this.ChatTile3.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(243)))), ((int)(((byte)(252)))));
            this.ChatTile3.OnHoverTextColor = System.Drawing.Color.Black;
            this.ChatTile3.selected = false;
            this.ChatTile3.Size = new System.Drawing.Size(385, 78);
            this.ChatTile3.TabIndex = 16;
            this.ChatTile3.Text = "Elad Ezra";
            this.ChatTile3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ChatTile3.Textcolor = System.Drawing.Color.Black;
            this.ChatTile3.TextFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChatTile3.BackColorChanged += new System.EventHandler(this.ChatTile3_BackColorChanged);
            this.ChatTile3.Click += new System.EventHandler(this.ChatTile3_Click);
            // 
            // LastMessage4
            // 
            this.LastMessage4.BackColor = System.Drawing.Color.Transparent;
            this.LastMessage4.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.LastMessage4.Location = new System.Drawing.Point(81, 452);
            this.LastMessage4.Name = "LastMessage4";
            this.LastMessage4.Size = new System.Drawing.Size(286, 36);
            this.LastMessage4.TabIndex = 19;
            this.LastMessage4.Text = "ZEBROCY-CET-IL";
            this.LastMessage4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LastMessage4.Click += new System.EventHandler(this.label3_Click);
            // 
            // ChatTile4
            // 
            this.ChatTile4.Activecolor = System.Drawing.Color.Transparent;
            this.ChatTile4.BackColor = System.Drawing.Color.Transparent;
            this.ChatTile4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ChatTile4.BorderRadius = 0;
            this.ChatTile4.ButtonText = "Cyber Notifications";
            this.ChatTile4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ChatTile4.DisabledColor = System.Drawing.Color.Gray;
            this.ChatTile4.ForeColor = System.Drawing.Color.Black;
            this.ChatTile4.Iconcolor = System.Drawing.Color.White;
            this.ChatTile4.Iconimage = global::WindowsFormsApplication1.Properties.Resources.c_removebg;
            this.ChatTile4.Iconimage_right = null;
            this.ChatTile4.Iconimage_right_Selected = null;
            this.ChatTile4.Iconimage_Selected = null;
            this.ChatTile4.IconMarginLeft = 0;
            this.ChatTile4.IconMarginRight = 0;
            this.ChatTile4.IconRightVisible = true;
            this.ChatTile4.IconRightZoom = 0D;
            this.ChatTile4.IconVisible = true;
            this.ChatTile4.IconZoom = 92D;
            this.ChatTile4.IsTab = false;
            this.ChatTile4.Location = new System.Drawing.Point(-1, 420);
            this.ChatTile4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChatTile4.Name = "ChatTile4";
            this.ChatTile4.Normalcolor = System.Drawing.Color.Transparent;
            this.ChatTile4.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(243)))), ((int)(((byte)(252)))));
            this.ChatTile4.OnHoverTextColor = System.Drawing.Color.Black;
            this.ChatTile4.selected = false;
            this.ChatTile4.Size = new System.Drawing.Size(385, 78);
            this.ChatTile4.TabIndex = 18;
            this.ChatTile4.Text = "Cyber Notifications";
            this.ChatTile4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ChatTile4.Textcolor = System.Drawing.Color.Black;
            this.ChatTile4.TextFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChatTile4.BackColorChanged += new System.EventHandler(this.ChatTile4_BackColorChanged);
            this.ChatTile4.Click += new System.EventHandler(this.ChatTile4_Click);
            // 
            // LastMessage5
            // 
            this.LastMessage5.BackColor = System.Drawing.Color.Transparent;
            this.LastMessage5.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.LastMessage5.Location = new System.Drawing.Point(81, 540);
            this.LastMessage5.Name = "LastMessage5";
            this.LastMessage5.Size = new System.Drawing.Size(286, 36);
            this.LastMessage5.TabIndex = 21;
            this.LastMessage5.Text = "כנראה 15:30";
            this.LastMessage5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ChatTile5
            // 
            this.ChatTile5.Activecolor = System.Drawing.Color.Transparent;
            this.ChatTile5.BackColor = System.Drawing.Color.Transparent;
            this.ChatTile5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ChatTile5.BorderRadius = 0;
            this.ChatTile5.ButtonText = "Cyber 2018-2019";
            this.ChatTile5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ChatTile5.DisabledColor = System.Drawing.Color.Gray;
            this.ChatTile5.ForeColor = System.Drawing.Color.Black;
            this.ChatTile5.Iconcolor = System.Drawing.Color.White;
            this.ChatTile5.Iconimage = global::WindowsFormsApplication1.Properties.Resources.c2_removebg;
            this.ChatTile5.Iconimage_right = null;
            this.ChatTile5.Iconimage_right_Selected = null;
            this.ChatTile5.Iconimage_Selected = null;
            this.ChatTile5.IconMarginLeft = 0;
            this.ChatTile5.IconMarginRight = 0;
            this.ChatTile5.IconRightVisible = true;
            this.ChatTile5.IconRightZoom = 0D;
            this.ChatTile5.IconVisible = true;
            this.ChatTile5.IconZoom = 93D;
            this.ChatTile5.IsTab = false;
            this.ChatTile5.Location = new System.Drawing.Point(-1, 508);
            this.ChatTile5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChatTile5.Name = "ChatTile5";
            this.ChatTile5.Normalcolor = System.Drawing.Color.Transparent;
            this.ChatTile5.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(243)))), ((int)(((byte)(252)))));
            this.ChatTile5.OnHoverTextColor = System.Drawing.Color.Black;
            this.ChatTile5.selected = false;
            this.ChatTile5.Size = new System.Drawing.Size(385, 78);
            this.ChatTile5.TabIndex = 20;
            this.ChatTile5.Text = "Cyber 2018-2019";
            this.ChatTile5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ChatTile5.Textcolor = System.Drawing.Color.Black;
            this.ChatTile5.TextFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChatTile5.BackColorChanged += new System.EventHandler(this.ChatTile5_BackColorChanged);
            this.ChatTile5.Click += new System.EventHandler(this.ChatTile5_Click);
            // 
            // LastMessage6
            // 
            this.LastMessage6.BackColor = System.Drawing.Color.Transparent;
            this.LastMessage6.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.LastMessage6.Location = new System.Drawing.Point(81, 628);
            this.LastMessage6.Name = "LastMessage6";
            this.LastMessage6.Size = new System.Drawing.Size(286, 36);
            this.LastMessage6.TabIndex = 23;
            this.LastMessage6.Text = "New login. Dear Tal, we detected";
            this.LastMessage6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ChatTile6
            // 
            this.ChatTile6.Activecolor = System.Drawing.Color.Transparent;
            this.ChatTile6.BackColor = System.Drawing.Color.Transparent;
            this.ChatTile6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ChatTile6.BorderRadius = 0;
            this.ChatTile6.ButtonText = "Telegram";
            this.ChatTile6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ChatTile6.DisabledColor = System.Drawing.Color.Gray;
            this.ChatTile6.ForeColor = System.Drawing.Color.Black;
            this.ChatTile6.Iconcolor = System.Drawing.Color.White;
            this.ChatTile6.Iconimage = global::WindowsFormsApplication1.Properties.Resources.t3_removebg;
            this.ChatTile6.Iconimage_right = null;
            this.ChatTile6.Iconimage_right_Selected = null;
            this.ChatTile6.Iconimage_Selected = null;
            this.ChatTile6.IconMarginLeft = 0;
            this.ChatTile6.IconMarginRight = 0;
            this.ChatTile6.IconRightVisible = true;
            this.ChatTile6.IconRightZoom = 0D;
            this.ChatTile6.IconVisible = true;
            this.ChatTile6.IconZoom = 93D;
            this.ChatTile6.IsTab = false;
            this.ChatTile6.Location = new System.Drawing.Point(-1, 596);
            this.ChatTile6.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChatTile6.Name = "ChatTile6";
            this.ChatTile6.Normalcolor = System.Drawing.Color.Transparent;
            this.ChatTile6.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(243)))), ((int)(((byte)(252)))));
            this.ChatTile6.OnHoverTextColor = System.Drawing.Color.Black;
            this.ChatTile6.selected = false;
            this.ChatTile6.Size = new System.Drawing.Size(385, 78);
            this.ChatTile6.TabIndex = 22;
            this.ChatTile6.Text = "Telegram";
            this.ChatTile6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ChatTile6.Textcolor = System.Drawing.Color.Black;
            this.ChatTile6.TextFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChatTile6.BackColorChanged += new System.EventHandler(this.ChatTile6_BackColorChanged);
            this.ChatTile6.Click += new System.EventHandler(this.ChatTile6_Click);
            // 
            // SearchBox
            // 
            this.SearchBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.SearchBox.BorderColorFocused = System.Drawing.Color.Transparent;
            this.SearchBox.BorderColorIdle = System.Drawing.Color.Transparent;
            this.SearchBox.BorderColorMouseHover = System.Drawing.Color.Transparent;
            this.SearchBox.BorderThickness = 3;
            this.SearchBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.SearchBox.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchBox.ForeColor = System.Drawing.Color.DarkGray;
            this.SearchBox.isPassword = false;
            this.SearchBox.Location = new System.Drawing.Point(39, 91);
            this.SearchBox.Margin = new System.Windows.Forms.Padding(4);
            this.SearchBox.Name = "SearchBox";
            this.SearchBox.Size = new System.Drawing.Size(336, 38);
            this.SearchBox.TabIndex = 24;
            this.SearchBox.Text = "Search";
            this.SearchBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.SearchBox.Enter += new System.EventHandler(this.SearchBox_Enter);
            this.SearchBox.Leave += new System.EventHandler(this.SearchBox_Leave);
            // 
            // MessageBox
            // 
            this.MessageBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.MessageBox.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MessageBox.ForeColor = System.Drawing.Color.DarkGray;
            this.MessageBox.HintForeColor = System.Drawing.Color.DarkGray;
            this.MessageBox.HintText = "Write a message...";
            this.MessageBox.isPassword = false;
            this.MessageBox.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(183)))), ((int)(((byte)(228)))));
            this.MessageBox.LineIdleColor = System.Drawing.Color.DarkGray;
            this.MessageBox.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(183)))), ((int)(((byte)(228)))));
            this.MessageBox.LineThickness = 2;
            this.MessageBox.Location = new System.Drawing.Point(49, 9);
            this.MessageBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MessageBox.Name = "MessageBox";
            this.MessageBox.Size = new System.Drawing.Size(577, 46);
            this.MessageBox.TabIndex = 25;
            this.MessageBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuImageButton1
            // 
            this.bunifuImageButton1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton1.Image = global::WindowsFormsApplication1.Properties.Resources.attachment;
            this.bunifuImageButton1.ImageActive = null;
            this.bunifuImageButton1.Location = new System.Drawing.Point(6, 19);
            this.bunifuImageButton1.Name = "bunifuImageButton1";
            this.bunifuImageButton1.Size = new System.Drawing.Size(36, 36);
            this.bunifuImageButton1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton1.TabIndex = 26;
            this.bunifuImageButton1.TabStop = false;
            this.bunifuImageButton1.Zoom = 10;
            this.bunifuImageButton1.Click += new System.EventHandler(this.bunifuImageButton1_Click);
            // 
            // SendButton
            // 
            this.SendButton.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SendButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(159)))), ((int)(((byte)(218)))));
            this.SendButton.Location = new System.Drawing.Point(636, 28);
            this.SendButton.Name = "SendButton";
            this.SendButton.Size = new System.Drawing.Size(51, 23);
            this.SendButton.TabIndex = 27;
            this.SendButton.Text = "SEND";
            this.SendButton.Click += new System.EventHandler(this.SendButton_Click);
            // 
            // Friend
            // 
            this.Friend.Controls.Add(this.Extract);
            this.Friend.Controls.Add(this.FriendText);
            this.Friend.Controls.Add(this.FriendPicture);
            this.Friend.Controls.Add(this.FriendAudio);
            this.Friend.Controls.Add(this.FriendPic);
            this.Friend.Location = new System.Drawing.Point(499, 117);
            this.Friend.Name = "Friend";
            this.Friend.Size = new System.Drawing.Size(596, 231);
            this.Friend.TabIndex = 28;
            this.Friend.Visible = false;
            // 
            // Extract
            // 
            this.Extract.Image = global::WindowsFormsApplication1.Properties.Resources.send;
            this.Extract.Location = new System.Drawing.Point(538, 94);
            this.Extract.Name = "Extract";
            this.Extract.Size = new System.Drawing.Size(49, 46);
            this.Extract.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Extract.TabIndex = 31;
            this.Extract.TabStop = false;
            // 
            // FriendText
            // 
            this.FriendText.Location = new System.Drawing.Point(69, 67);
            this.FriendText.Name = "FriendText";
            this.FriendText.Size = new System.Drawing.Size(246, 160);
            this.FriendText.TabIndex = 3;
            this.FriendText.Text = "label1";
            // 
            // FriendPicture
            // 
            this.FriendPicture.Image = global::WindowsFormsApplication1.Properties.Resources.waiting_time;
            this.FriendPicture.ImageLocation = "D:\\Cyber\\Project\\waiting-time.png";
            this.FriendPicture.Location = new System.Drawing.Point(321, 67);
            this.FriendPicture.Name = "FriendPicture";
            this.FriendPicture.Size = new System.Drawing.Size(211, 160);
            this.FriendPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.FriendPicture.TabIndex = 2;
            this.FriendPicture.TabStop = false;
            // 
            // FriendAudio
            // 
            this.FriendAudio.ImageLocation = "D:\\Cyber\\Project\\audio.jpg";
            this.FriendAudio.Location = new System.Drawing.Point(69, 4);
            this.FriendAudio.Name = "FriendAudio";
            this.FriendAudio.Size = new System.Drawing.Size(463, 57);
            this.FriendAudio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.FriendAudio.TabIndex = 1;
            this.FriendAudio.TabStop = false;
            this.FriendAudio.Click += new System.EventHandler(this.FriendAudio_Click);
            // 
            // FriendPic
            // 
            this.FriendPic.Image = global::WindowsFormsApplication1.Properties.Resources.contact;
            this.FriendPic.Location = new System.Drawing.Point(4, 4);
            this.FriendPic.Name = "FriendPic";
            this.FriendPic.Size = new System.Drawing.Size(58, 57);
            this.FriendPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FriendPic.TabIndex = 0;
            this.FriendPic.TabStop = false;
            // 
            // Me
            // 
            this.Me.Controls.Add(this.SendSteg);
            this.Me.Controls.Add(this.MyText);
            this.Me.Controls.Add(this.Picture);
            this.Me.Controls.Add(this.Audio);
            this.Me.Controls.Add(this.SelfPic);
            this.Me.Location = new System.Drawing.Point(499, 364);
            this.Me.Name = "Me";
            this.Me.Size = new System.Drawing.Size(596, 231);
            this.Me.TabIndex = 29;
            this.Me.Visible = false;
            // 
            // SendSteg
            // 
            this.SendSteg.Image = global::WindowsFormsApplication1.Properties.Resources.send;
            this.SendSteg.Location = new System.Drawing.Point(538, 88);
            this.SendSteg.Name = "SendSteg";
            this.SendSteg.Size = new System.Drawing.Size(49, 46);
            this.SendSteg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.SendSteg.TabIndex = 30;
            this.SendSteg.TabStop = false;
            this.SendSteg.Click += new System.EventHandler(this.SendSteg_Click);
            // 
            // MyText
            // 
            this.MyText.Location = new System.Drawing.Point(69, 67);
            this.MyText.Name = "MyText";
            this.MyText.Size = new System.Drawing.Size(246, 160);
            this.MyText.TabIndex = 3;
            this.MyText.Text = "Secret Text To Send";
            // 
            // Picture
            // 
            this.Picture.Image = global::WindowsFormsApplication1.Properties.Resources.waiting_time;
            this.Picture.ImageLocation = "D:\\Cyber\\Project\\waiting-time.png";
            this.Picture.Location = new System.Drawing.Point(321, 67);
            this.Picture.Name = "Picture";
            this.Picture.Size = new System.Drawing.Size(211, 160);
            this.Picture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Picture.TabIndex = 2;
            this.Picture.TabStop = false;
            // 
            // Audio
            // 
            this.Audio.Image = global::WindowsFormsApplication1.Properties.Resources.audio;
            this.Audio.Location = new System.Drawing.Point(69, 4);
            this.Audio.Name = "Audio";
            this.Audio.Size = new System.Drawing.Size(463, 57);
            this.Audio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Audio.TabIndex = 1;
            this.Audio.TabStop = false;
            this.Audio.Click += new System.EventHandler(this.Audio_Click);
            // 
            // SelfPic
            // 
            this.SelfPic.Image = global::WindowsFormsApplication1.Properties.Resources.contact;
            this.SelfPic.Location = new System.Drawing.Point(4, 4);
            this.SelfPic.Name = "SelfPic";
            this.SelfPic.Size = new System.Drawing.Size(58, 57);
            this.SelfPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.SelfPic.TabIndex = 0;
            this.SelfPic.TabStop = false;
            // 
            // ChatName
            // 
            this.ChatName.AutoSize = true;
            this.ChatName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(130)))), ((int)(((byte)(163)))));
            this.ChatName.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChatName.ForeColor = System.Drawing.Color.Transparent;
            this.ChatName.Location = new System.Drawing.Point(402, 20);
            this.ChatName.Name = "ChatName";
            this.ChatName.Size = new System.Drawing.Size(140, 26);
            this.ChatName.TabIndex = 30;
            this.ChatName.Text = "TeleStegram";
            this.ChatName.Visible = false;
            this.ChatName.MouseHover += new System.EventHandler(this.label1_MouseHover);
            // 
            // DropMenu
            // 
            this.DropMenu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.DropMenu.Controls.Add(this.bunifuFlatButton2);
            this.DropMenu.Controls.Add(this.bunifuFlatButton1);
            this.DropMenu.Location = new System.Drawing.Point(-1, 0);
            this.DropMenu.MaximumSize = new System.Drawing.Size(385, 191);
            this.DropMenu.MinimumSize = new System.Drawing.Size(385, 64);
            this.DropMenu.Name = "DropMenu";
            this.DropMenu.Size = new System.Drawing.Size(385, 64);
            this.DropMenu.TabIndex = 31;
            // 
            // bunifuFlatButton2
            // 
            this.bunifuFlatButton2.Activecolor = System.Drawing.Color.White;
            this.bunifuFlatButton2.BackColor = System.Drawing.Color.White;
            this.bunifuFlatButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton2.BorderRadius = 0;
            this.bunifuFlatButton2.ButtonText = "About";
            this.bunifuFlatButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuFlatButton2.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton2.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton2.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton2.Iconimage")));
            this.bunifuFlatButton2.Iconimage_right = null;
            this.bunifuFlatButton2.Iconimage_right_Selected = null;
            this.bunifuFlatButton2.Iconimage_Selected = null;
            this.bunifuFlatButton2.IconMarginLeft = 0;
            this.bunifuFlatButton2.IconMarginRight = 0;
            this.bunifuFlatButton2.IconRightVisible = true;
            this.bunifuFlatButton2.IconRightZoom = 0D;
            this.bunifuFlatButton2.IconVisible = true;
            this.bunifuFlatButton2.IconZoom = 60D;
            this.bunifuFlatButton2.IsTab = false;
            this.bunifuFlatButton2.Location = new System.Drawing.Point(0, 127);
            this.bunifuFlatButton2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bunifuFlatButton2.Name = "bunifuFlatButton2";
            this.bunifuFlatButton2.Normalcolor = System.Drawing.Color.White;
            this.bunifuFlatButton2.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(243)))), ((int)(((byte)(252)))));
            this.bunifuFlatButton2.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(130)))), ((int)(((byte)(163)))));
            this.bunifuFlatButton2.selected = false;
            this.bunifuFlatButton2.Size = new System.Drawing.Size(385, 65);
            this.bunifuFlatButton2.TabIndex = 1;
            this.bunifuFlatButton2.Text = "About";
            this.bunifuFlatButton2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton2.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(130)))), ((int)(((byte)(163)))));
            this.bunifuFlatButton2.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // bunifuFlatButton1
            // 
            this.bunifuFlatButton1.Activecolor = System.Drawing.Color.White;
            this.bunifuFlatButton1.BackColor = System.Drawing.Color.White;
            this.bunifuFlatButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton1.BorderRadius = 0;
            this.bunifuFlatButton1.ButtonText = "Settings";
            this.bunifuFlatButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuFlatButton1.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton1.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton1.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton1.Iconimage")));
            this.bunifuFlatButton1.Iconimage_right = null;
            this.bunifuFlatButton1.Iconimage_right_Selected = null;
            this.bunifuFlatButton1.Iconimage_Selected = null;
            this.bunifuFlatButton1.IconMarginLeft = 0;
            this.bunifuFlatButton1.IconMarginRight = 0;
            this.bunifuFlatButton1.IconRightVisible = true;
            this.bunifuFlatButton1.IconRightZoom = 0D;
            this.bunifuFlatButton1.IconVisible = true;
            this.bunifuFlatButton1.IconZoom = 70D;
            this.bunifuFlatButton1.IsTab = false;
            this.bunifuFlatButton1.Location = new System.Drawing.Point(0, 64);
            this.bunifuFlatButton1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bunifuFlatButton1.Name = "bunifuFlatButton1";
            this.bunifuFlatButton1.Normalcolor = System.Drawing.Color.White;
            this.bunifuFlatButton1.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(243)))), ((int)(((byte)(252)))));
            this.bunifuFlatButton1.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(130)))), ((int)(((byte)(163)))));
            this.bunifuFlatButton1.selected = false;
            this.bunifuFlatButton1.Size = new System.Drawing.Size(385, 65);
            this.bunifuFlatButton1.TabIndex = 0;
            this.bunifuFlatButton1.Text = "Settings";
            this.bunifuFlatButton1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton1.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(130)))), ((int)(((byte)(163)))));
            this.bunifuFlatButton1.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // MessageListener
            // 
            this.MessageListener.Enabled = true;
            this.MessageListener.Interval = 2000;
            this.MessageListener.Tick += new System.EventHandler(this.MessageListener_Tick);
            // 
            // BarPanel
            // 
            this.BarPanel.Controls.Add(this.MessageBox);
            this.BarPanel.Controls.Add(this.bunifuImageButton1);
            this.BarPanel.Controls.Add(this.SendButton);
            this.BarPanel.Location = new System.Drawing.Point(443, 617);
            this.BarPanel.Name = "BarPanel";
            this.BarPanel.Size = new System.Drawing.Size(692, 59);
            this.BarPanel.TabIndex = 32;
            this.BarPanel.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(144F, 144F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1178, 688);
            this.Controls.Add(this.BarPanel);
            this.Controls.Add(this.ChatName);
            this.Controls.Add(this.Me);
            this.Controls.Add(this.Friend);
            this.Controls.Add(this.LastMessage6);
            this.Controls.Add(this.ChatTile6);
            this.Controls.Add(this.LastMessage5);
            this.Controls.Add(this.ChatTile5);
            this.Controls.Add(this.LastMessage4);
            this.Controls.Add(this.ChatTile4);
            this.Controls.Add(this.LastMessage3);
            this.Controls.Add(this.ChatTile3);
            this.Controls.Add(this.LastMessage2);
            this.Controls.Add(this.ChatTile2);
            this.Controls.Add(this.MenuButton);
            this.Controls.Add(this.Title);
            this.Controls.Add(this.MiddleMenu);
            this.Controls.Add(this.LeftMenu);
            this.Controls.Add(this.DropMenu);
            this.Controls.Add(this.LastMessage1);
            this.Controls.Add(this.ChatTile1);
            this.Controls.Add(this.bunifuSeparator1);
            this.Controls.Add(this.SearchBox);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.listView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MainMenuStrip = this.LeftMenu;
            this.Name = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.MenuButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).EndInit();
            this.Friend.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Extract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FriendPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FriendAudio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FriendPic)).EndInit();
            this.Me.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SendSteg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Audio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfPic)).EndInit();
            this.DropMenu.ResumeLayout(false);
            this.BarPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip LeftMenu;
        private System.Windows.Forms.MenuStrip MiddleMenu;
        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.PictureBox MenuButton;
        private System.Windows.Forms.ListView listView1;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Bunifu.Framework.UI.BunifuFlatButton ChatTile1;
        private System.Windows.Forms.Label LastMessage1;
        private System.Windows.Forms.Label LastMessage2;
        private Bunifu.Framework.UI.BunifuFlatButton ChatTile2;
        private System.Windows.Forms.Label LastMessage3;
        private Bunifu.Framework.UI.BunifuFlatButton ChatTile3;
        private System.Windows.Forms.Label LastMessage4;
        private Bunifu.Framework.UI.BunifuFlatButton ChatTile4;
        private System.Windows.Forms.Label LastMessage5;
        private Bunifu.Framework.UI.BunifuFlatButton ChatTile5;
        private System.Windows.Forms.Label LastMessage6;
        private Bunifu.Framework.UI.BunifuFlatButton ChatTile6;
        private Bunifu.Framework.UI.BunifuMetroTextbox SearchBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox MessageBox;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton1;
        private System.Windows.Forms.Label SendButton;
        private System.Windows.Forms.Panel Friend;
        private System.Windows.Forms.PictureBox FriendPicture;
        private System.Windows.Forms.PictureBox FriendAudio;
        private System.Windows.Forms.PictureBox FriendPic;
        private System.Windows.Forms.Panel Me;
        private System.Windows.Forms.Label MyText;
        private System.Windows.Forms.PictureBox Picture;
        private System.Windows.Forms.PictureBox Audio;
        private System.Windows.Forms.PictureBox SelfPic;
        private System.Windows.Forms.Label FriendText;
        private System.Windows.Forms.PictureBox SendSteg;
        private System.Windows.Forms.PictureBox Extract;
        private System.Windows.Forms.Label ChatName;
        private System.Windows.Forms.Panel DropMenu;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton2;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton1;
        private System.Windows.Forms.Timer MessageListener;
        private System.Windows.Forms.Panel BarPanel;
    }
}

