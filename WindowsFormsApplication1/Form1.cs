﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using TeleSharp;
using TeleSharp.TL;
using TeleSharp.TL.Messages;
using TeleSharp.TL.Users;
using TLSharp.Core;
using TLSharp.Core.Network;
using TLSharp.Core.Requests;
using TLSharp.Core.Utils;
using System.IO;
using System.Diagnostics;
using TeleSharp.TL.Updates;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        private TelegramClient client;
        private string audio_location;
        private Bunifu.Framework.UI.BunifuFlatButton btn;
        private Label lbl;
        private TLDialogs dl;
        private TLUser CurUser;
        private TLState state;
        private TLUser[] users;
        private const string PYTHON_LOCATION = "C:/Python27/python.exe";
        private const string LSBEXTRACT_LOCATION = "C:/Users/talsa/Desktop/skillz/SkillzChallenges/WindowsFormsApplication1/WindowsFormsApplication1/Properties/Assets/LSBExtract.py";
        private const string LSBSTEG_LOCATION = "C:/Users/talsa/Desktop/skillz/SkillzChallenges/WindowsFormsApplication1/WindowsFormsApplication1/Properties/Assets/LSBSteg.py";

        public Form1(TelegramClient client)
        {
            InitializeComponent();
            this.client = client;
            this.users = new TLUser[6];
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void LeftMenu_MouseHover(object sender, EventArgs e)
        {
            LeftMenu.BackColor = Color.FromArgb(73, 119, 153);
            Title.BackColor = Color.FromArgb(73, 119, 153);
            MenuButton.BackColor = Color.FromArgb(73, 119, 153);
        }

        private void Title_MouseHover(object sender, EventArgs e)
        {
            LeftMenu.BackColor = Color.FromArgb(73, 119, 153);
            Title.BackColor = Color.FromArgb(73, 119, 153);
            MenuButton.BackColor = Color.FromArgb(73, 119, 153);
        }

        private void MenuButton_MouseHover(object sender, EventArgs e)
        {
            LeftMenu.BackColor = Color.FromArgb(73, 119, 153);
            Title.BackColor = Color.FromArgb(73, 119, 153);
            MenuButton.BackColor = Color.FromArgb(73, 119, 153);
        }

        private void LeftMenu_MouseLeave(object sender, EventArgs e)
        {
            LeftMenu.BackColor = Color.FromArgb(86, 130, 163);
            Title.BackColor = Color.FromArgb(86, 130, 163);
            MenuButton.BackColor = Color.FromArgb(86, 130, 163);
        }

        private void Title_MouseLeave(object sender, EventArgs e)
        {
            LeftMenu.BackColor = Color.FromArgb(86, 130, 163);
            Title.BackColor = Color.FromArgb(86, 130, 163);
            MenuButton.BackColor = Color.FromArgb(86, 130, 163);
        }

        private void MenuButton_MouseLeave(object sender, EventArgs e)
        {
            LeftMenu.BackColor = Color.FromArgb(86, 130, 163);
            Title.BackColor = Color.FromArgb(86, 130, 163);
            MenuButton.BackColor = Color.FromArgb(86, 130, 163);
        }

        private void MiddleMenu_MouseHover(object sender, EventArgs e)
        {
            MiddleMenu.BackColor = Color.FromArgb(73, 119, 153);
            ChatName.BackColor = Color.FromArgb(73, 119, 153);
        }

        private void MiddleMenu_MouseLeave(object sender, EventArgs e)
        {
            MiddleMenu.BackColor = Color.FromArgb(86, 130, 163);
            ChatName.BackColor = Color.FromArgb(86, 130, 163);
        }

        private async void Form1_Load(object sender, EventArgs e)
        {
            MessageListener.Stop();
            var dialogs = (TLDialogs)await this.client.GetUserDialogsAsync();
            this.dl = dialogs;
            var firstUser = (TLUser)dialogs.Users.ElementAt(0);
            ChatTile1.Text = firstUser.FirstName + " " + firstUser.LastName;
            LastMessage1.Text = MessageById(dialogs, firstUser.Id);
            this.users[0] = firstUser;
            /***var Photo = (TLUserProfilePhoto)firstUser.Photo;
            var SmallPhoto = (TLFileLocation)Photo.PhotoSmall;
            var picture = await this.client.GetFile(new TLInputFileLocation()
            {
                LocalId = SmallPhoto.LocalId,
                Secret = SmallPhoto.Secret,
                VolumeId = SmallPhoto.VolumeId
            }, 1024 * 256);
            using (var m = new MemoryStream(picture.Bytes))
            {
                var img = Image.FromStream(m);
                ChatTile1.Iconimage = img;
            }***/
            var secondUser = (TLUser)dialogs.Users.ElementAt(1);
            ChatTile2.Text = secondUser.FirstName + " " + secondUser.LastName;
            LastMessage2.Text = MessageById(dialogs, secondUser.Id);
            this.users[1] = secondUser;
            /***var Photo2 = (TLUserProfilePhoto)secondUser.Photo;
            var SmallPhoto2 = (TLFileLocation)Photo2.PhotoSmall;
            var picture2 = await this.client.GetFile(new TLInputFileLocation()
            {
                LocalId = SmallPhoto2.LocalId,
                Secret = SmallPhoto2.Secret,
                VolumeId = SmallPhoto2.VolumeId
            }, 0, 0);
            using (var m = new MemoryStream(picture2.Bytes))
            {
                var img = Image.FromStream(m);
                ChatTile2.Iconimage = img;
            }***/
            var thirdUser = (TLUser)dialogs.Users.ElementAt(2);
            ChatTile3.Text = thirdUser.FirstName + " " + thirdUser.LastName;
            LastMessage3.Text = MessageById(dialogs, thirdUser.Id);
            this.users[2] = thirdUser;
            /***var Photo3 = (TLUserProfilePhoto)thirdUser.Photo;
            var SmallPhoto3 = (TLFileLocation)Photo3.PhotoSmall;
            var picture3 = await this.client.GetFile(new TLInputFileLocation()
            {
                LocalId = SmallPhoto3.LocalId,
                Secret = SmallPhoto3.Secret,
                VolumeId = SmallPhoto3.VolumeId
            }, 1024 * 256);
            using (var m = new MemoryStream(picture3.Bytes))
            {
                var img = Image.FromStream(m);
                ChatTile3.Iconimage = img;
            }***/
            var fourthUser = (TLUser)dialogs.Users.ElementAt(3);
            ChatTile4.Text = fourthUser.FirstName + " " + fourthUser.LastName;
            LastMessage4.Text = MessageById(dialogs, fourthUser.Id);
            this.users[3] = fourthUser;
            /***var Photo4 = (TLUserProfilePhoto)fourthUser.Photo;
            var SmallPhoto4 = (TLFileLocation)Photo4.PhotoSmall;
            var picture4 = await this.client.GetFile(new TLInputFileLocation()
            {
                LocalId = SmallPhoto4.LocalId,
                Secret = SmallPhoto4.Secret,
                VolumeId = SmallPhoto4.VolumeId
            }, 0, 0);
            using (var m = new MemoryStream(picture4.Bytes))
            {
                var img = Image.FromStream(m);
                ChatTile4.Iconimage = img;
            }***/
            var fifthUser = (TLUser)dialogs.Users.ElementAt(4);
            ChatTile5.Text = fifthUser.FirstName + " " + fifthUser.LastName;
            LastMessage5.Text = MessageById(dialogs, fifthUser.Id);
            this.users[4] = fifthUser;
            /***var Photo5 = (TLUserProfilePhoto)fifthUser.Photo;
            var SmallPhoto5 = (TLFileLocation)Photo5.PhotoSmall;
            var picture5 = await this.client.GetFile(new TLInputFileLocation()
            {
                LocalId = SmallPhoto5.LocalId,
                Secret = SmallPhoto5.Secret,
                VolumeId = SmallPhoto5.VolumeId
            }, 0, 0);
            using (var m = new MemoryStream(picture5.Bytes))
            {
                var img = Image.FromStream(m);
                ChatTile5.Iconimage = img;
            }***/
            var sixthUser = (TLUser)dialogs.Users.ElementAt(5);
            ChatTile6.Text = sixthUser.FirstName + " " + sixthUser.LastName;
            LastMessage6.Text = MessageById(dialogs, sixthUser.Id);
            this.users[5] = sixthUser;
            /***var Photo6 = (TLUserProfilePhoto)sixthUser.Photo;
            var SmallPhoto6 = (TLFileLocation)Photo6.PhotoSmall;
            var picture6 = await this.client.GetFile(new TLInputFileLocation()
            {
                LocalId = SmallPhoto6.LocalId,
                Secret = SmallPhoto6.Secret,
                VolumeId = SmallPhoto6.VolumeId
            }, 0, 0);
            using (var m = new MemoryStream(picture6.Bytes))
            {
                var img = Image.FromStream(m);
                ChatTile6.Iconimage = img;
            }***/
            /***var Photo7 = (TLUserProfilePhoto)this.user.Photo;
            var SmallPhoto7 = (TLFileLocation)Photo7.PhotoSmall;
            var picture7 = await this.client.GetFile(new TLInputFileLocation()
            {
                LocalId = SmallPhoto7.LocalId,
                Secret = SmallPhoto7.Secret,
                VolumeId = SmallPhoto7.VolumeId
            }, 0, 0);
            using (var m = new MemoryStream(picture7.Bytes))
            {
                var img = Image.FromStream(m);
                SelfPic.Image = img;
            }***/
            this.state = await this.client.SendRequestAsync<TLState>(new TLRequestGetState());
            MessageListener.Start();
        }

        private string MessageById(TeleSharp.TL.Messages.TLDialogs dialogs, int id)
        {
            var To = new TLPeerUser();
            for (int i = 0; i < dialogs.Messages.Count; i++)
            {
                try
                {
                    var Message = (TLMessage)dialogs.Messages.ElementAt(i);
                    try
                    {
                        To = (TLPeerUser)Message.ToId;
                    }
                    catch
                    {
                        continue;
                    }
                    if (To.UserId == id || Message.FromId == id)
                    {
                        return Message.Message;
                    }
                }
                catch
                {
                    continue;
                }
            }
            return "";
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void SearchBox_Enter(object sender, EventArgs e)
        {
            if (SearchBox.Text == "Search")
            {
                SearchBox.Text = "";
                SearchBox.ForeColor = Color.Black;
            }
        }

        private void SearchBox_Leave(object sender, EventArgs e)
        {
            if (SearchBox.Text == "")
            {
                SearchBox.Text = "Search";
                SearchBox.ForeColor = Color.DarkGray;
            }
        }

        private void ChatTile1_Click(object sender, EventArgs e)
        {
            Friend.Visible = true;
            Me.Visible = true;
            BarPanel.Visible = true;
            ChatName.Text = ChatTile1.Text;
            ChatName.Visible = true;
            FriendPic.Image = ChatTile1.Iconimage;
            FriendText.Text = LastMessage1.Text;
            ChatTile1.Normalcolor = Color.FromArgb(86, 130, 163);
            LastMessage1.BackColor = Color.FromArgb(86, 130, 163);
            ChatTile1.Iconcolor = Color.FromArgb(86, 130, 163);
            if (btn != null)
            {
                btn.Normalcolor = Color.White;
                lbl.BackColor = Color.White;
                btn.Iconcolor = Color.White;
            }
            btn = ChatTile1;
            lbl = LastMessage1;
            this.CurUser = (TLUser)this.dl.Users.ElementAt(0);
        }

        public string run_cmd(string args, string python)
        {
            Process proc = new Process();
            proc.StartInfo.FileName = PYTHON_LOCATION;
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.UseShellExecute = false;

            proc.StartInfo.Arguments = string.Concat(python, " ", args);
            proc.Start();

            StreamReader sReader = proc.StandardOutput;
            string output = sReader.ReadToEnd();

            proc.WaitForExit();
            return output;
        }

        private string ExtractSteg(string location)
        {
            string result = run_cmd(location, LSBEXTRACT_LOCATION);
            return result;
        }

        private void SendButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Text != "")
            {
                MyText.Text = MessageBox.Text;
                MessageBox.Text = "";
            }
        }

        private void Extract_Click(object sender, EventArgs e)
        {

        }

        private void ChatTile2_Click(object sender, EventArgs e)
        {
            Friend.Visible = true;
            Me.Visible = true;
            BarPanel.Visible = true;
            ChatName.Text = ChatTile2.Text;
            ChatName.Visible = true;
            FriendPic.Image = ChatTile2.Iconimage;
            FriendText.Text = LastMessage2.Text;
            ChatTile2.Normalcolor = Color.FromArgb(86, 130, 163);
            LastMessage2.BackColor = Color.FromArgb(86, 130, 163);
            ChatTile2.Iconcolor = Color.FromArgb(86, 130, 163);
            if (btn != null)
            {
                btn.Normalcolor = Color.White;
                lbl.BackColor = Color.White;
                btn.Iconcolor = Color.White;
            }
            btn = ChatTile2;
            lbl = LastMessage2;
            this.CurUser = (TLUser)this.dl.Users.ElementAt(1);
        }

        private void ChatTile3_Click(object sender, EventArgs e)
        {
            Friend.Visible = true;
            Me.Visible = true;
            BarPanel.Visible = true;
            ChatName.Text = ChatTile3.Text;
            ChatName.Visible = true;
            FriendPic.Image = ChatTile3.Iconimage;
            FriendText.Text = LastMessage3.Text;
            ChatTile3.Normalcolor = Color.FromArgb(86, 130, 163);
            LastMessage3.BackColor = Color.FromArgb(86, 130, 163);
            ChatTile3.Iconcolor = Color.FromArgb(86, 130, 163);
            if (btn != null)
            {
                btn.Normalcolor = Color.White;
                lbl.BackColor = Color.White;
                btn.Iconcolor = Color.White;
            }
            btn = ChatTile3;
            lbl = LastMessage3;
            this.CurUser = (TLUser)this.dl.Users.ElementAt(2);
        }

        private void ChatTile4_Click(object sender, EventArgs e)
        {
            Friend.Visible = true;
            Me.Visible = true;
            BarPanel.Visible = true;
            ChatName.Text = ChatTile4.Text;
            ChatName.Visible = true;
            FriendPic.Image = ChatTile4.Iconimage;
            FriendText.Text = LastMessage4.Text;
            ChatTile4.Normalcolor = Color.FromArgb(86, 130, 163);
            LastMessage4.BackColor = Color.FromArgb(86, 130, 163);
            ChatTile4.Iconcolor = Color.FromArgb(86, 130, 163);
            if (btn != null)
            {
                btn.Normalcolor = Color.White;
                lbl.BackColor = Color.White;
                btn.Iconcolor = Color.White;
            }
            btn = ChatTile4;
            lbl = LastMessage4;
            this.CurUser = (TLUser)this.dl.Users.ElementAt(3);
        }

        private void ChatTile5_Click(object sender, EventArgs e)
        {
            Friend.Visible = true;
            Me.Visible = true;
            BarPanel.Visible = true;
            ChatName.Text = ChatTile5.Text;
            ChatName.Visible = true;
            FriendPic.Image = ChatTile5.Iconimage;
            FriendText.Text = LastMessage5.Text;
            ChatTile5.Normalcolor = Color.FromArgb(86, 130, 163);
            LastMessage5.BackColor = Color.FromArgb(86, 130, 163);
            ChatTile5.Iconcolor = Color.FromArgb(86, 130, 163);
            if (btn != null)
            {
                btn.Normalcolor = Color.White;
                lbl.BackColor = Color.White;
                btn.Iconcolor = Color.White;
            }
            btn = ChatTile5;
            lbl = LastMessage5;
            this.CurUser = (TLUser)this.dl.Users.ElementAt(4);
        }

        private void ChatTile6_Click(object sender, EventArgs e)
        {
            Friend.Visible = true;
            Me.Visible = true;
            BarPanel.Visible = true;
            ChatName.Text = ChatTile6.Text;
            ChatName.Visible = true;
            FriendPic.Image = ChatTile6.Iconimage;
            FriendText.Text = LastMessage6.Text;
            ChatTile6.Normalcolor = Color.FromArgb(86, 130, 163);
            LastMessage6.BackColor = Color.FromArgb(86, 130, 163);
            ChatTile6.Iconcolor = Color.FromArgb(86, 130, 163);
            if (btn != null)
            {
                btn.Normalcolor = Color.White;
                lbl.BackColor = Color.White;
                btn.Iconcolor = Color.White;
            }
            btn = ChatTile6;
            lbl = LastMessage6;
            this.CurUser = (TLUser)this.dl.Users.ElementAt(5);
        }

        private void bunifuImageButton1_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.Filter = "Wave files only (*.wav)|*.wav|Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            if (fd.ShowDialog() == DialogResult.OK)
            {
                if (Path.GetExtension(fd.FileName) != ".wav")
                {
                    Picture.Image = Image.FromFile(fd.FileName);
                }
                else
                {
                    Audio.Image = Properties.Resources.color_audio;
                    this.audio_location = fd.FileName;
                }
            }
        }

        private void Audio_Click(object sender, EventArgs e)
        {
            try
            {
                System.Media.SoundPlayer sp = new System.Media.SoundPlayer();
                sp.SoundLocation = this.audio_location;
                sp.Load();
                sp.PlaySync();
            }
            catch (Win32Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        private void label1_MouseHover(object sender, EventArgs e)
        {

        }

        bool isCollapsed = false;

        private void LeftMenu_Click(object sender, EventArgs e)
        {
            if (isCollapsed)
            {
                LeftMenu.BackColor = Color.FromArgb(86, 130, 163);
                Title.BackColor = Color.FromArgb(86, 130, 163);
                MenuButton.BackColor = Color.FromArgb(86, 130, 163);
                DropMenu.Size = DropMenu.MinimumSize;
                MenuButton.Image = Properties.Resources._2;
                isCollapsed = false;
            }
            else
            {
                LeftMenu.BackColor = Color.FromArgb(73, 119, 153);
                Title.BackColor = Color.FromArgb(73, 119, 153);
                MenuButton.BackColor = Color.FromArgb(73, 119, 153);
                DropMenu.Size = DropMenu.MaximumSize;
                MenuButton.Image = Properties.Resources.exit;
                isCollapsed = true;
            }
        }

        private async void SendSteg_Click(object sender, EventArgs e)
        {
            string result = run_cmd(this.audio_location + " " + MyText.Text, LSBSTEG_LOCATION);
            System.Windows.Forms.MessageBox.Show("Done");
            MessageListener.Stop();
            var fileResult = (TLInputFile)await this.client.UploadFile("audio.wav", new StreamReader(result));

            TLDocumentAttributeFilename s = new TLDocumentAttributeFilename();
            s.FileName = "audio.wav";
            TLVector<TLAbsDocumentAttribute> attrs = new TLVector<TLAbsDocumentAttribute>();
            attrs.Add(s);
            await client.SendUploadedDocument(
                new TLInputPeerUser() { UserId = this.CurUser.Id },
                fileResult,
                "audio file",
                "wav:audio/x-wav",
                attrs);
            MessageListener.Start();
            this.state = await this.client.SendRequestAsync<TLState>(new TLRequestGetState());
        }

        private async void MessageListener_Tick(object sender, EventArgs e)
        {
            var req = new TLRequestGetDifference() { Date = this.state.Date, Pts = this.state.Pts, Qts = this.state.Qts };
            var adiff = await this.client.SendRequestAsync<TLAbsDifference>(req);
            if (!(adiff is TLDifferenceEmpty))
            {
                var diff = adiff as TLDifference;
                if (diff.NewMessages.Count < 1)
                {
                    return;
                }
                MessageListener.Stop();
                var msg = (TLMessage)diff.NewMessages.ElementAt(0);
                if (msg.Message != "")
                {
                    GetMessage(msg.FromId.GetValueOrDefault(), msg.Message);
                }
                else
                {
                    TLMessageMediaDocument media = (TLMessageMediaDocument)msg.Media;
                    TLDocument doc = (TLDocument)media.Document;

                    TeleSharp.TL.Upload.TLFile resFile = new TeleSharp.TL.Upload.TLFile();
                    int filePart = 512 * 1024;
                    int offset = 0;

                    using (FileStream fs = new FileStream("C:\\Users\\talsa\\Desktop\\skillz\\SkillzChallenges\\WindowsFormsApplication1\\WindowsFormsApplication1\\Properties\\Assets\\recieved.wav", FileMode.Create))
                    {
                        
                        while (offset < doc.Size)
                        {
                            resFile = await this.client.GetFile(
                                new TLInputDocumentFileLocation()
                                {
                                    AccessHash = doc.AccessHash,
                                    Id = doc.Id,
                                    Version = doc.Version
                                },
                                filePart, offset);
                            fs.Write(resFile.Bytes, 0, resFile.Bytes.Length);
                            offset += filePart;
                        }
                    }
                    //;System.IO.File.WriteAllBytes("D:/Cyber/Project/recieved.wav", resFile.Bytes);
                    FriendAudio.Image = Properties.Resources.color_audio;
                    string result = ExtractSteg("C:\\Users\\talsa\\Desktop\\skillz\\SkillzChallenges\\WindowsFormsApplication1\\WindowsFormsApplication1\\Properties\\Assets\\recieved.wav");
                    GetMessage(msg.FromId.GetValueOrDefault(), result);
                }
                this.state = await this.client.SendRequestAsync<TLState>(new TLRequestGetState());
                MessageListener.Start();
            }
        }

        public void GetMessage(int id, string text, string photo = "") 
        {
            if (photo == "")
            {
                if (id == this.users[0].Id)
                {
                    LastMessage1.Text = text;
                    ChatTile1_Click(ChatTile1, EventArgs.Empty);
                }
                else if (id == this.users[1].Id)
                {
                    LastMessage2.Text = text;
                    ChatTile2_Click(ChatTile2, EventArgs.Empty);
                }
                else if (id == this.users[2].Id)
                {
                    LastMessage3.Text = text;
                    ChatTile3_Click(ChatTile3, EventArgs.Empty);
                }
                else if (id == this.users[3].Id)
                {
                    LastMessage4.Text = text;
                    ChatTile3_Click(ChatTile3, EventArgs.Empty);
                }
                else if (id == this.users[4].Id)
                {
                    LastMessage5.Text = text;
                    ChatTile4_Click(ChatTile4, EventArgs.Empty);
                }
                else if (id == this.users[5].Id)
                {
                    LastMessage6.Text = text;
                    ChatTile5_Click(ChatTile5, EventArgs.Empty);
                }
            }
        }

        private void MenuButton_Click(object sender, EventArgs e)
        {
            if (isCollapsed)
            {
                LeftMenu.BackColor = Color.FromArgb(86, 130, 163);
                Title.BackColor = Color.FromArgb(86, 130, 163);
                MenuButton.BackColor = Color.FromArgb(86, 130, 163);
                DropMenu.Size = DropMenu.MinimumSize;
                MenuButton.Image = Properties.Resources._2;
                isCollapsed = false;
            }
            else
            {
                LeftMenu.BackColor = Color.FromArgb(73, 119, 153);
                Title.BackColor = Color.FromArgb(73, 119, 153);
                MenuButton.BackColor = Color.FromArgb(73, 119, 153);
                DropMenu.Size = DropMenu.MaximumSize;
                MenuButton.Image = Properties.Resources.exit;
                isCollapsed = true;
            }
        }

        private void Title_Click(object sender, EventArgs e)
        {
            if (isCollapsed)
            {
                LeftMenu.BackColor = Color.FromArgb(86, 130, 163);
                Title.BackColor = Color.FromArgb(86, 130, 163);
                MenuButton.BackColor = Color.FromArgb(86, 130, 163);
                DropMenu.Size = DropMenu.MinimumSize;
                MenuButton.Image = Properties.Resources._2;
                isCollapsed = false;
            }
            else
            {
                LeftMenu.BackColor = Color.FromArgb(73, 119, 153);
                Title.BackColor = Color.FromArgb(73, 119, 153);
                MenuButton.BackColor = Color.FromArgb(73, 119, 153);
                DropMenu.Size = DropMenu.MaximumSize;
                MenuButton.Image = Properties.Resources.exit;
                isCollapsed = true;
            }
        }

        private void ChatTile1_BackColorChanged(object sender, EventArgs e)
        {
            if (!(ChatTile1.BackColor == Color.FromArgb(86, 130, 163)))
            {
                if (ChatTile1.BackColor == Color.Transparent)
                {
                    LastMessage1.BackColor = Color.Transparent;
                    ChatTile1.Iconcolor = Color.Transparent;
                }
                else
                {
                    if (LastMessage1.BackColor == Color.FromArgb(236, 243, 252))
                        LastMessage1.BackColor = Color.Transparent;
                    else
                        LastMessage1.BackColor = Color.FromArgb(236, 243, 252);
                }
            }
            else
            {
                LastMessage1.BackColor = Color.FromArgb(86, 130, 163);
            }
        }

        private void ChatTile2_BackColorChanged(object sender, EventArgs e)
        {
            if (!(ChatTile2.BackColor == Color.FromArgb(86, 130, 163)))
            {
                if (ChatTile2.BackColor == Color.Transparent)
                {
                    LastMessage2.BackColor = Color.Transparent;
                    ChatTile2.Iconcolor = Color.Transparent;
                }
                else
                {
                    if (LastMessage2.BackColor == Color.FromArgb(236, 243, 252))
                        LastMessage2.BackColor = Color.Transparent;
                    else
                        LastMessage2.BackColor = Color.FromArgb(236, 243, 252);
                }
            }
            else
            {
                LastMessage2.BackColor = Color.FromArgb(86, 130, 163);
            }
        }

        private void ChatTile3_BackColorChanged(object sender, EventArgs e)
        {
            if (!(ChatTile3.BackColor == Color.FromArgb(86, 130, 163)))
            {
                if (ChatTile3.BackColor == Color.Transparent)
                {
                    LastMessage3.BackColor = Color.Transparent;
                    ChatTile3.Iconcolor = Color.Transparent;
                }
                else
                {
                    if (LastMessage3.BackColor == Color.FromArgb(236, 243, 252))
                        LastMessage3.BackColor = Color.Transparent;
                    else
                        LastMessage3.BackColor = Color.FromArgb(236, 243, 252);
                }
            }
            else
            {
                LastMessage3.BackColor = Color.FromArgb(86, 130, 163);
            }
        }

        private void ChatTile4_BackColorChanged(object sender, EventArgs e)
        {
            if (!(ChatTile4.BackColor == Color.FromArgb(86, 130, 163)))
            {
                if (ChatTile4.BackColor == Color.Transparent)
                {
                    LastMessage4.BackColor = Color.Transparent;
                    ChatTile4.Iconcolor = Color.Transparent;
                }
                else
                {
                    if (LastMessage4.BackColor == Color.FromArgb(236, 243, 252))
                        LastMessage4.BackColor = Color.Transparent;
                    else
                        LastMessage4.BackColor = Color.FromArgb(236, 243, 252);
                }
            }
            else
            {
                LastMessage4.BackColor = Color.FromArgb(86, 130, 163);
            }
        }

        private void ChatTile5_BackColorChanged(object sender, EventArgs e)
        {
            if (!(ChatTile5.BackColor == Color.FromArgb(86, 130, 163)))
            {
                if (ChatTile5.BackColor == Color.Transparent)
                {
                    LastMessage5.BackColor = Color.Transparent;
                    ChatTile5.Iconcolor = Color.Transparent;
                }
                else
                {
                    if (LastMessage5.BackColor == Color.FromArgb(236, 243, 252))
                        LastMessage5.BackColor = Color.Transparent;
                    else
                        LastMessage5.BackColor = Color.FromArgb(236, 243, 252);
                }
            }
            else
            {
                LastMessage5.BackColor = Color.FromArgb(86, 130, 163);
            }
        }

        private void ChatTile6_BackColorChanged(object sender, EventArgs e)
        {
            if (!(ChatTile6.BackColor == Color.FromArgb(86, 130, 163)))
            {
                if (ChatTile6.BackColor == Color.Transparent)
                {
                    LastMessage6.BackColor = Color.Transparent;
                    ChatTile6.Iconcolor = Color.Transparent;
                }
                else
                {
                    if (LastMessage6.BackColor == Color.FromArgb(236, 243, 252))
                        LastMessage6.BackColor = Color.Transparent;
                    else
                        LastMessage6.BackColor = Color.FromArgb(236, 243, 252);
                }
            }
            else
            {
                LastMessage6.BackColor = Color.FromArgb(86, 130, 163);
            }
        }

        private void FriendAudio_Click(object sender, EventArgs e)
        {
            try
            {
                System.Media.SoundPlayer sp = new System.Media.SoundPlayer();
                sp.SoundLocation = "C:\\Users\\talsa\\Desktop\\skillz\\SkillzChallenges\\WindowsFormsApplication1\\WindowsFormsApplication1\\Properties\\Assets\\recieved.wav";
                sp.Load();
                sp.PlaySync();
            }
            catch (Win32Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }
    }
}
