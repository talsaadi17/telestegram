import wave
import sys

try:
    audio_path = sys.argv[1]
    data = ' '.join(sys.argv[2:])
except:
    sys.stdout.write('Args not valid')
    sys.stdout.flush()
    sys.exit(0)

song = wave.open(audio_path, mode='rb')
# Read frames and convert to byte array
frame_bytes = bytearray(list(song.readframes(song.getnframes())))

data = data + int((len(frame_bytes)-(len(data)*8*8))/8) *'#'
# Convert text to bit array
bits = list(map(int, ''.join([bin(ord(i)).lstrip('0b').rjust(8,'0') for i in data])))

# Replace LSB of each byte of the audio data by one bit from the text bit array
for i, bit in enumerate(bits):
    frame_bytes[i] = (frame_bytes[i] & 254) | bit
# Get the modified bytes
frame_modified = bytes(frame_bytes)

# Write bytes to a new wave audio file
new_name = audio_path.split('.')[0] + '_embedded.' + audio_path.split('.')[1]
fd = wave.open(new_name, 'wb')
fd.setparams(song.getparams())
fd.writeframes(frame_modified)
fd.close()
song.close()
sys.stdout.write(new_name)
sys.stdout.flush()
sys.exit(0)