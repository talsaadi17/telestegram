import wave
import sys

try:
    audio_path = sys.argv[1]
except:
    sys.stdout.write('Args not valid')
    sys.stdout.flush()
    sys.exit(0)

song = wave.open(audio_path, mode='rb')
# Convert audio to byte array
frame_bytes = bytearray(list(song.readframes(song.getnframes())))

# Extract the LSB of each byte
extracted = [frame_bytes[i] & 1 for i in range(len(frame_bytes))]
# Convert byte array back to string
string = "".join(chr(int("".join(map(str,extracted[i:i+8])),2)) for i in range(0,len(extracted),8))
# Cut off at the filler characters
decoded = string.split("###")[0]

song.close()
# return the extracted text
sys.stdout.write(decoded)
sys.stdout.flush()
sys.exit(0)



